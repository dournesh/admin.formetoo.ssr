import React from 'react'

import Dashboard from '@src/components/content/dashboard'
import Profile from '@src/containers/content/profile'
import Logs from '@src/containers/content/logs'
import ResourceCreateEditTemplate from '@src/containers/content/resource-create-edit-template'
import ResourcesLayout from '@src/containers/content/resources-layout'
import RemoveLayout from '@src/containers/content/remove-layout'

import listRoutes from './list'

export default (location, root) => {
	let resources = [
		'categories',
		'products',
		'orders',
		'users',
		'clients',
		'attributes',
		'attribute-sets',
		'tabs',
		'tab-sets',
		'statuses',
		'roles',
		'photos'
	]
	let routes = [
		{
			path: root,
			exact: true,
			component: Dashboard
		}, {
			path: root + 'profile',
			exact: true,
			component: () => <Profile
				location={location}
				resources='photos'
			/>
		}, {
			path: root + 'logs',
			exact: true,
			component: () => <Logs/>
		}
	]

	listRoutes.forEach(route => {
		routes.push({
			path: root + route.resource,
			exact: true,
			component: () => <ResourcesLayout
				path={location}
				title={route.title}
				columns={route.columns}
				filters={route.filters}
			/>
		})
		routes.push({
			path: `${root}${route.resource}/create`,
			exact: true,
			component: () => <ResourceCreateEditTemplate
				resource={route.resource}
				structure={route.structure}
				action='create'
			/>
		})
		routes.push({
			path: `${root}${route.resource}/:id`,
			exact: true,
			component: (props) => <ResourceCreateEditTemplate
				resource={route.resource}
				structure={route.structure}
				action='edit'
				{...props}
			/>
		})
		routes.push({
			path: `${root}${route.resource}/:id/copy`,
			exact: true,
			component: (props) => <ResourceCreateEditTemplate
				resource={route.resource}
				structure={route.structure}
				copy
				action='copy'
				{...props}
			/>
		})
	})

	resources.forEach(resource => {
		routes.push({
			path: root + resource + '/:id/delete',
			exact: true,
			component: () => <RemoveLayout
				location={location}
				resources={resource}
			/>
		})
	})
	return routes
}